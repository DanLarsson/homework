cmake_minimum_required(VERSION 3.10)
project(Calendar2)

set(CMAKE_CXX_STANDARD 17)

add_executable(Calendar2 main.cpp calendar.hxx utils.hxx)