#include "gtest/gtest.h"
#include "../Date.hxx"

using namespace std;
using namespace std::string_literals;
using testing::Test;


TEST(date, firstOfMay) {
    Date d{2018, 5, 1};
    EXPECT_EQ(d.year(), 2018U);
    EXPECT_EQ(d.month(), 5U);
    EXPECT_EQ(d.day(), 1U);
    EXPECT_EQ(d.weekday(), 2U); //0=Sunday, 2==Tuesday
}

TEST(date, addDaysWithinMonth) {
    Date d{2018, 5, 1};
    EXPECT_EQ(d.month(), 5U);
    EXPECT_EQ(d.day(), 1U);

    auto d2 = d.plusDays(14);
    EXPECT_EQ(d2.month(), 5U);
    EXPECT_EQ(d2.day(), 15U);
}

TEST(date, addDaysOutsideMonth) {
    Date d{2018, 5, 1};
    EXPECT_EQ(d.month(), 5U);
    EXPECT_EQ(d.day(), 1U);

    auto d2 = d.plusDays(35);
    EXPECT_EQ(d2.month(), 6U);
    EXPECT_EQ(d2.day(), 5U);
}

TEST(date, subDaysWithinMonth) {
    Date d{2018, 5, 31};
    EXPECT_EQ(d.month(), 5U);
    EXPECT_EQ(d.day(), 31U);

    auto d2 = d.plusDays(-20);
    EXPECT_EQ(d2.month(), 5U);
    EXPECT_EQ(d2.day(), 11U);
}

TEST(date, subDaysOutsideMonth) {
    Date d{2018, 5, 15};
    EXPECT_EQ(d.month(), 5U);
    EXPECT_EQ(d.day(), 15U);

    auto d2 = d.plusDays(-30);
    EXPECT_EQ(d2.month(), 4U);
    EXPECT_EQ(d2.day(), 15U);
}

TEST(dateExpr, add) {
    Date d{2018, 5, 15};

    auto d2 = d + 10_days;
    EXPECT_EQ(d2.day(), 25U);
    EXPECT_EQ(d2.month(), 5U);
}

TEST(dateExpr, sub) {
    Date d{2018, 5, 15};

    auto d2 = d - 10_days;
    EXPECT_EQ(d2.day(), 5U);
    EXPECT_EQ(d2.month(), 5U);
}

TEST(dateBnd, first) {
    Date d{2018, 5, 15};
    auto d2 = d.first();
    EXPECT_EQ(d2.day(), 1U);
}

TEST(dateBnd, lastOfMay) {
    Date d{2018, 5, 15};
    auto d2 = d.last();
    EXPECT_EQ(d2.day(), 31U);
}

TEST(dateBnd, lastOfApr) {
    Date d{2018, 4, 15};
    auto d2 = d.last();
    EXPECT_EQ(d2.day(), 30U);
}

TEST(dateBnd, lastOfFeb) {
    Date d{2018, 2, 15};
    auto d2 = d.last();
    EXPECT_EQ(d2.day(), 28U);
}

TEST(dateBnd, lastOfFebLeap) {
    Date d{2016, 2, 15};
    auto d2 = d.last();
    EXPECT_EQ(d2.day(), 29U);
}

