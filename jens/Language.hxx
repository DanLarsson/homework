#pragma once

#include <string>
#include <vector>
#include <initializer_list>

using namespace std;


class Language {
    vector<string> days;
    vector<string> months;
    unsigned       weekdayOffset = 1;

public:
    Language(initializer_list<string> days, initializer_list<string> months)
            : days{days.begin(), days.end()}, months{months.begin(), months.end()} {
    }

    string weekday(unsigned dayno) const {
        const auto N = days.size();
        return days[(dayno - 1 + weekdayOffset + N) % N];
    }

    string month(unsigned monthno) const {
        return months[monthno - 1];
    }

    unsigned int getWeekdayOffset() const {
        return weekdayOffset;
    }

    void setWeekdayOffset(unsigned int weekdayOffset) {
        Language::weekdayOffset = weekdayOffset;
    }

};


