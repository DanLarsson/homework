#pragma once

#include <string>
#include <ctime>

using namespace std;
using namespace std::string_literals;


class Date {
    tm current;

public:
    Date() {
        auto ts = time(nullptr);
        localtime_r(&ts, &current);
    }

    Date(unsigned year, unsigned month, unsigned day) {
        tm info{};
        info.tm_year = year - 1900;
        info.tm_mon  = month - 1;
        info.tm_mday = day;

        auto ts = mktime(&info);
        localtime_r(&ts, &current);
    }

    ~Date() = default;
    Date(const Date&) = default;
    Date& operator=(const Date&)= default;

    unsigned year() const {
        return static_cast<unsigned>(current.tm_year) + 1900;
    }

    unsigned month() const {
        return static_cast<unsigned>(current.tm_mon) + 1;
    }

    unsigned day() const {
        return static_cast<unsigned>(current.tm_mday);
    }

    unsigned weekday() const {
        return static_cast<unsigned>(current.tm_wday);
    }

    Date plusDays(int offset) const {
        return {year(), month(), day() + offset};
    }

    Date first() const {
        return {year(), month(), 1};
    }

    Date last() const {
        auto date = first().plusDays(32);
        while (date.month() != month()) {
            date = date.plusDays(-1);
        }
        return {year(), month(), date.day()};
    }

};


inline Date operator+(Date date, unsigned numDays) {
    return date.plusDays(numDays);
}

inline Date operator-(Date date, unsigned numDays) {
    return date.plusDays(-numDays);
}

constexpr unsigned long long int operator "" _days(unsigned long long int n) {
    return n;
}

