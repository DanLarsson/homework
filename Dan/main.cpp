#include<iostream>
#include <ctime>
#include<string>

using namespace std;
int is_leap_year (int y) {
    return (y % 4) ? 0 : ((y % 100) ? 1 : !(y % 400));
}

int main(int nArgs, char* args[])
{
    string month;
    int year;
    if (nArgs>=2) month = args[1];
    if (nArgs==3) year = stoi(args[2]);
    int days_this_month=0;
    string namn[]={"January","February","March","April","May","June","July","August","September","October","November","December"};
    int nr_of_days_in_month[]={31,28,31,30,31,30,31,31,30,31,30,31};
    time_t t = time(nullptr);
    tm* tidPtr=localtime(&t);
    int k = 0;
    while (namn[k]!=month) k++;//find number of month
    if (k>=0 && k<12) tidPtr->tm_mon=k;
    if (year>1900 && year<2038) tidPtr->tm_year=year-1900;
    mktime(tidPtr);

    cout <<namn[(tidPtr->tm_mon)]<<" "<< (tidPtr->tm_year)+1900<<endl;
    cout <<"Mon Tue Wed Thu Fri Sat Sun"<<endl;
    int day1=(7+tidPtr->tm_wday-(tidPtr->tm_mday))%7;//find 1:st day of months weekday
    days_this_month=nr_of_days_in_month[(tidPtr->tm_mon)];
    if((tidPtr->tm_mon)==1)  days_this_month+=is_leap_year(tidPtr->tm_year);
    for (int j=0;j<day1;j++) cout << "    ";
    int i=1;
    while(i<=days_this_month){
        if (i<10) cout << " ";
        cout << " "<<i<<" ";
        if ((i+day1)%7==0) cout<<endl;
        i++;
    }
    cout << endl;
    return 0;
}

