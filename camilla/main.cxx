#include <iostream>
#include <string>
#include <ctime>
#include "Calender.hxx"
#include "Utilities.hxx"

using namespace std;

int main(int argn, char* argv[]) {

    unsigned fromMonth = Utilities::currentMonth();
    unsigned year = Utilities::currentYear();
    unsigned toMonth = 0;
    unsigned startDay = 1; // Monday
    string lang = Utilities::eng;

    for (unsigned i = 1; i < static_cast<unsigned>(argn); ++i) {
        string arg = argv[i];
        string val;
        if ((i + 1) >= static_cast<unsigned>(argn) || arg == "-h") {
            cerr << "Current year and month used" << endl;
            cerr << "Usage is for from month use flag: -f 1-12" << endl;
            cerr << "Usage is for to month use flag: -t 1-12 and > -f" << endl;
            cerr << "Usage is for year use flag: -y 1900-xxxx" << endl;
            cerr << "Usage is for start day use flag: -s 1-7" << endl;
            cerr << "Usage is for language use flag: -l eng or swe" << endl;
            break;
        } else {
            val = argv[++i];
        }
        if (arg == "-f") {
            Utilities::parseMonth(val, fromMonth);
        }
        else if (arg == "-t") {
            Utilities::parseMonth(val, toMonth);
        }
        else if (arg == "-y") {
            Utilities::parseYear(val, year);
        }
        else if (arg == "-s") {
            Utilities::parseStartDay(val, startDay);
        }
        else if (arg == "-l") {
            Utilities::parseLanguage(val, lang);
        }
    }

    if(toMonth < fromMonth)
        toMonth = fromMonth;

    Lang* l;
    if(lang == Utilities::eng)
        l = new Eng();
    else if(lang == Utilities::swe)
        l = new Swe();

    Calender cal {year, fromMonth, toMonth, startDay, l};
    cout << cal << endl;

    delete l;
    return 0;
}



