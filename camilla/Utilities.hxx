#ifndef HOMEWORK2_UTILITIES_HXX
#define HOMEWORK2_UTILITIES_HXX
#include <iostream>
#include <string>
#include <cctype>
#include "Calender.hxx"

namespace Utilities {
    const unsigned numDays = 7;
    const unsigned numMonths = 12;
    const unsigned minYear = 1900;
    const std::string eng = "ENG";
    const std::string swe = "SWE";

    unsigned currentMonth() {
        time_t t = time(NULL);
        struct tm* currTime = localtime(&t);
        // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
        return currTime->tm_mon + 1;
    }

    unsigned currentYear() {
        time_t t = time(NULL);
        struct tm* currTime = localtime(&t);
        // Year is # years since 1900
        return currTime->tm_year + 1900;
    }

    std::string toUpper(std::string s) {
        for (unsigned i = 0; i < s.length(); ++i )
            s[i] = toupper(s[i]);
        return s;
    }

    void parseMonth (const std::string& value, unsigned& m) {
        unsigned uValue = 0;
        uValue = stoi(value);
        if (uValue >= 1 && uValue <= numMonths) {
            m = uValue;
        }
    }

    void parseYear (const std::string& value, unsigned& y) {
        unsigned uValue = 0;
        uValue = stoi(value);
        if(uValue >= minYear)
            y = uValue;
    }

    void parseStartDay (const std::string& value, unsigned& s) {
        unsigned uValue = 0;
        uValue = stoi(value);
        if (uValue >= 1 && uValue <= numDays) {
            s = uValue;
        }
    }

    void parseLanguage (const std::string& value, std::string& l) {
        std::string upper= toUpper(value);
        if(upper == swe)
            l = upper;
        else
            l = eng;
    }

}

#endif //HOMEWORK2_UTILITIES_HXX