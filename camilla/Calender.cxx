//
// Created by camilla on 2018-05-07.
//
#include <string>
#include <vector>
#include <iomanip>
#include "Calender.hxx"

using namespace std;
const unsigned numDays = 7;

Calender::Calender (const unsigned& year,
                    const unsigned& fromMonth,
                    const unsigned& toMonth,
                    const unsigned& startDay,
                    const Lang* lang = nullptr) :
        year{year},
        fromMonth{fromMonth},
        toMonth{toMonth},
        startDay{startDay},
        lang(lang)
{

}

unsigned Calender::getNumOfDays(const unsigned& month) const {
    switch (month) {
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            return 31;
        case 4 :
        case 6 :
        case 9 :
        case 11 :
            return 30;
        case 2 : {
            return __isleap(getYear()) ? 29 : 28;
        }
        default : return 0;
    }
}

const vector<unsigned> Calender::getDayOrder() const {
    vector<unsigned> days;
    for (unsigned i = getStartDay(); i <= numDays; ++i) {
        days.push_back(i);
    }
    for (unsigned i = 1; i < getStartDay(); ++i) {
        days.push_back(i);
    }
    return days;
}

unsigned Calender::getFirstDayInMonth(const unsigned& month) const {
    unsigned day = 1;
    unsigned y = getYear() - (14 - month) / 12;
    unsigned m = month + 12 * ((14 - month) / 12) - 2;
    unsigned firstDay = (day + y + y / 4 - y / 100 + y / 400 + (31 * m / 12)) % 7;
    return firstDay;
}

ostream& operator<<(ostream& os, const Calender& cal) {
    // Loop through the months to display
    vector<unsigned> days = cal.getDayOrder();
    for (unsigned i = cal.getFrom(); i <= cal.getTo(); ++i) {
        // Month and Year
        os << cal.getMonth(i) << " " << cal.yearToString() << endl;
        //Loop through the week-days
        for(unsigned j = 0; j < numDays; ++j) {
            os  << cal.getDay(days[j]) ;
            os << setw(5) << setfill(' ');
        }
        os << endl;
        os << setw(0);
        unsigned firstDay = cal.getFirstDayInMonth(i);
        unsigned count = 1;
        for (unsigned k = 1; k <= numDays; ++k) {
            if (firstDay <= k) {
                os << count;
                ++count;
            }
            else os << " ";
            os << setw(5) << setfill(' ');
        }
        os << endl;
        os << setw(0);

        unsigned c = 1;
        for (unsigned k = count; k <= cal.getNumOfDays(i); ++k) {
            os << k;
            if(k==9)
                os << setw(6) << setfill(' ');
            else os << setw(5) << setfill(' ');
            if(c % numDays == 0) {
                os << endl;
                os << setw(0);
            }
            ++c;
        }
        os << endl;
        os << endl;
    }
    return os;
}

