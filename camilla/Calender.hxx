//
// Created by camilla on 2018-05-07.
//
#ifndef HOMEWORK2_CALENDER_HXX
#define HOMEWORK2_CALENDER_HXX
#include <iostream>
#include <string>
#include <vector>

struct Lang {
    Lang(const std::string& lang) : lang(lang) {}
    virtual ~Lang() {};
    virtual const std::string& getLang() const { return lang; }
    virtual const std::string getMonth(const unsigned& i) const = 0;
    virtual const std::string getDay(const unsigned& i) const  = 0;
private:
    std::string lang;
};

struct Eng: public Lang {
        Eng()
                : Lang{"ENG"} {}

        ~Eng() {}
    const std::string getMonth (const unsigned& i) const {
        switch (i) {
            case 1 : return "January";
            case 2 : return "February";
            case 3 : return "March";
            case 4 : return "April";
            case 5 : return "May";
            case 6 : return "June";
            case 7 : return "July";
            case 8 : return "August";
            case 9 : return "September";
            case 10 : return "October";
            case 11 : return "November";
            case 12 : return "December";
            default : return "Unknown";
        }
    }

    const std::string getDay(const unsigned& i) const {
        switch (i) {
            case 1 : return "Mon";
            case 2 : return "Tue";
            case 3 : return "Wed";
            case 4 : return "Thu";
            case 5 : return "Fri";
            case 6 : return "Sat";
            case 7 : return "Sun";
            default : return "Unknown";
        }
    }
};

struct Swe: public Lang {
    Swe()
            : Lang{"SWE"} {}

    ~Swe() {}

    const std::string getMonth (const unsigned& i) const {
        switch (i) {
            case 1 : return "Januari";
            case 2 : return "Februari";
            case 3 : return "Mars";
            case 4 : return "April";
            case 5 : return "Maj";
            case 6 : return "Juni";
            case 7 : return "Juli";
            case 8 : return "Augusti";
            case 9 : return "September";
            case 10 : return "Oktober";
            case 11 : return "November";
            case 12 : return "December";
            default : return "Unknown";
        }
    }

    const std::string getDay(const unsigned& i) const {
        switch (i) {
            case 1 : return "M\x86n";
            case 2 : return "Tis";
            case 3 : return "Ons";
            case 4 : return "Tor";
            case 5 : return "Fre";
            case 6 : return "L\x94r";
            case 7 : return "S\x94n";
            default : return "Unknown";
        }
    }
};

class Calender {
private:
    const unsigned year;
    const unsigned fromMonth;
    const unsigned toMonth;
    const unsigned startDay;
    const Lang* lang;
public:
    Calender (const unsigned& year,
              const unsigned& fromMonth,
              const unsigned& toMonth,
              const unsigned& startDay,
              const Lang* lang);
    ~Calender () {};
    const std::string getMonth(const unsigned& i) const { return lang->getMonth(i);}
    const std::string getDay(const unsigned& i) const { return lang->getDay(i);}
    unsigned getFrom() const { return fromMonth;}
    unsigned getTo() const { return toMonth;}
    unsigned getYear() const { return year;}
    std::string yearToString() const { return std::to_string(year);}
    unsigned getStartDay() const { return startDay;}
    unsigned getNumOfDays(const unsigned& month) const;
    const std::vector<unsigned> getDayOrder() const;
    unsigned getFirstDayInMonth(const unsigned& month) const;

};

std::ostream& operator<<(std::ostream& os, const Calender& cal);

#endif //HOMEWORK2_CALENDER_HXX